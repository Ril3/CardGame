You can find SS in "/public/images/inMyHead" drawing I've created in figma at the start of the project. That's logic that I've tryed to implement in the game but it's changed a bit because I've decided for it to be one player vs computer game. So instead of two players there are Player and Computer. 


How to play????

The top deck is the computer deck. The bottom deck is YOUR deck. Click on YOUR deck to flip the card, after that your card and computer card will be compared. If you have stronger card, you've won the round and you get to put your card back in the deck. If computer win, you lose your card and computer get to keep his card. If both cards are equally strong then it is a draw and neither player gets a card. The first to lose all the cards, lost the game as well.


It's responsive and playable on all devices.



![alt text](public/images/InMyHead.png) ;