const SUITS = ["♠", "♣", "♥", "♦"]
const VALUES = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]

export default class Deck {
  /**
   * In here I've called methode freshDeck who multiply two arrays and create new array with 52 cards
   * @param {String[]} cards 
   */
  constructor(cards = freshDeck()) {
    this.cards = cards
  }

  /**
   * I've used this to caunt cards in the deck.
   * @returns {Number}
   */
  get numberOfCards() {
    return this.cards.length
  }

  pull() {
    return this.cards.shift()
  }
  /**
   * In the file index.js there is const playerCard = playerDeck.pull() and const computerCard = computerDeck.pull() basicly in the place of parametre card i call playerDeck.pull() 
   * @param {Card} card 
   */
  push(card) {
    this.cards.push(card)
  }

  /**
   * this is shuffle the array/deck
   */
  shuffle() {
    for (let i = this.numberOfCards - 1; i > 0; i--) {
      const newIndex = Math.floor(Math.random() * (i + 1))
      const oldValue = this.cards[newIndex]
      this.cards[newIndex] = this.cards[i]
      this.cards[i] = oldValue
    }
  }
}


class Card {
  /**
   * @param {String} suit 
   * @param {String} value 
   */
  constructor(suit, value) {
    this.suit = suit
    this.value = value
  }

  /**
   * @returns color on the html element.
   */
  get color() {
    return this.suit === "♣" || this.suit === "♠" ? "black" : "red"
  }
  /**
   * @returns {HTMLElement} cardDiv and that is div that looks like card but it is dinamyc because it pull elements from the array  SUITS and VALUES that I've declared on startn and display them like one card.
   */
    getHTML() {
      const cardDiv = document.createElement("div")
      cardDiv.innerText = this.suit
      cardDiv.classList.add("card", this.color)
      cardDiv.dataset.value = `${this.value} ${this.suit}`
      return cardDiv
    }
  }
  /**
   * This will put together new deck/array
   * @returns {cards[]}
   */
  function freshDeck() {
    return SUITS.flatMap(suit => {
      return VALUES.map(value => {
        return new Card(suit, value)
      })
    })
}