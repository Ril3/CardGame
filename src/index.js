import Deck from "./Deck.js";

const CARD_VALUE_MAP = {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "10": 10,
    J: 11,
    Q: 12,
    K: 13,
    A: 14
  }

  //All the variables that I need

const computerCardSlot = document.querySelector(".computer-card-slot");
const playerCardSlot = document.querySelector(".player-card-slot");
const computerDeckElement = document.querySelector(".computer-deck");
const playerDeckElement = document.querySelector(".player-deck");
const text = document.querySelector(".text");

let playerDeck, computerDeck, inRound, stop;


//Controls
playerDeckElement.addEventListener("click", ()=>{
    if (stop) {
        startGame()
        return //this is just to not run code below
      }

    if(inRound){
      text.style.visibility = "hidden";
        cleanBeforeRound();
    }else {
        flipCards();
    }
})

startGame()
// function that create new deck for each player half of the cards and shuffle them
function startGame(){
    const deck = new Deck();
    deck.shuffle();
    
    const deckMidpoint = Math.ceil(deck.numberOfCards / 2);
    playerDeck = new Deck(deck.cards.slice(0, deckMidpoint));
    computerDeck = new Deck(deck.cards.slice(deckMidpoint, deck.numberOfCards));
    inRound = false;
    stop = false;

    cleanBeforeRound()
}

// function that clean everything before round
function cleanBeforeRound() {
    inRound = false;
    computerCardSlot.innerHTML = "";
    playerCardSlot.innerHTML = "";
    text.innerText = "";
  
    updateDeckCount()
  }

  function updateDeckCount() {
    computerDeckElement.innerText = computerDeck.numberOfCards
    playerDeckElement.innerText = playerDeck.numberOfCards
  }

  function flipCards() {
    inRound = true
  
    const playerCard = playerDeck.pull()
    const computerCard = computerDeck.pull()
  
    playerCardSlot.appendChild(playerCard.getHTML())
    computerCardSlot.appendChild(computerCard.getHTML())
  
    updateDeckCount()
    
    if (isRoundWinner(playerCard, computerCard)) {
        text.innerText = "You WON the round !!"
        text.style.visibility = "visible";
        playerDeck.push(playerCard)
      } else if (isRoundWinner(computerCard, playerCard)) {
        text.style.visibility = "visible";
        text.innerText = "You LOST the round !!"
        computerDeck.push(computerCard)
      } else {
        text.style.visibility = "visible";
        text.innerText = "Draw"
      }
    
      if (isGameOver(playerDeck)) {
        text.innerText = "You LOST the game!!!"
        stop = true
      } else if (isGameOver(computerDeck)) {
        text.innerText = "You WON the GAME!!!"
        stop = true
      }
  }


  
// This fucntion compare two cards
  function isRoundWinner(cardOne, cardTwo) {
    return CARD_VALUE_MAP[cardOne.value] > CARD_VALUE_MAP[cardTwo.value]
  }

  function isGameOver(deck) {
    return deck.numberOfCards === 0
  }
  